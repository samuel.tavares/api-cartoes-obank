package br.com.obank.pagamentos.services;

import br.com.obank.pagamentos.models.Pagamento;
import br.com.obank.pagamentos.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PagamentoService {


    @Autowired
    private PagamentoRepository pagamentoRepository;

    public Pagamento salvarPagamento(Pagamento pagamento){
        return pagamentoRepository.save(pagamento);
    }

    public Iterable<Pagamento> listarPagamentosPorCartao(int cartaoId){
        return pagamentoRepository.findAllByCartaoId(cartaoId);
    }
}
