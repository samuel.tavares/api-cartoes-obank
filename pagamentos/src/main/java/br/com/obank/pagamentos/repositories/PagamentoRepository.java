package br.com.obank.pagamentos.repositories;

import br.com.obank.pagamentos.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {
    Iterable<Pagamento> findAllByCartaoId(int id);
}

