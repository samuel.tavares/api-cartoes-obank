package br.com.obank.pagamentos.controllers;

import br.com.obank.pagamentos.models.Pagamento;
import br.com.obank.pagamentos.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/pagamento")
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Pagamento salvarPagamento(@RequestBody @Valid Pagamento pagamento){
        return pagamentoService.salvarPagamento(pagamento);
    }

    @GetMapping("/{id}")
    public Iterable<Pagamento> listarPagamentosPorCartao(@PathVariable (name = "id")int id){
        return pagamentoService.listarPagamentosPorCartao(id);
    }
}
