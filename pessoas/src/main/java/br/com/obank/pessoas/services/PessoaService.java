package br.com.obank.pessoas.services;

import br.com.obank.pessoas.models.Pessoa;
import br.com.obank.pessoas.repositories.PessoaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PessoaService {

    @Autowired
    private PessoaRepository pessoaRepository;

    public Pessoa salvarCliente(Pessoa pessoa){
        return pessoaRepository.save(pessoa);
    }

    public Pessoa buscarClientePorId(int id){
        Optional<Pessoa> cliente = pessoaRepository.findById(id);
        if(cliente.isPresent()){
            return cliente.get();
        }else{
            throw new RuntimeException("Cliente não encontrado");
        }
    }
}
