package br.com.obank.pessoas.repositories;

import br.com.obank.pessoas.models.Pessoa;
import org.springframework.data.repository.CrudRepository;

public interface PessoaRepository extends CrudRepository<Pessoa, Integer> {
}

