package br.com.obank.pessoas.controllers;

import br.com.obank.pessoas.models.Pessoa;
import br.com.obank.pessoas.services.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class PessoaController {

    @Autowired
    private PessoaService pessoaService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Pessoa salvarCliente(@RequestBody @Valid Pessoa cliente){
        return pessoaService.salvarCliente(cliente);
    }

    @GetMapping("/{id}")
    public Pessoa buscarClientePorId(@PathVariable(name = "id") int id ){
        try {
            return pessoaService.buscarClientePorId(id);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


}
