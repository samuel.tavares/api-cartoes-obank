package br.com.obank.cartoes.repositories;

import br.com.obank.cartoes.models.Cartao;
import org.springframework.data.repository.CrudRepository;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {
    Cartao findByNumero(String numero);
}
