package br.com.obank.cartoes.services;

import br.com.obank.cartoes.DTOs.AtivarDesativarCartaoDTO;
import br.com.obank.cartoes.models.Cartao;
import br.com.obank.cartoes.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    public Cartao criarCartao(Cartao cartao){
        return cartaoRepository.save(cartao);
    }

    public Cartao buscarCartaoPorId(int id){
        Optional<Cartao> cartaoDB = cartaoRepository.findById(id);
        if(!cartaoDB.isPresent()){
            throw new RuntimeException("Cartão não encontrado");
        }
        return cartaoDB.get();
    }

    public Cartao buscarCartaoPorNumero(String numeroCartao){
        Optional<Cartao> cartaoDB = Optional.ofNullable(cartaoRepository.findByNumero(numeroCartao));
        if(!cartaoDB.isPresent()){
            throw new RuntimeException("Cartão não encontrado");
        }
        return cartaoDB.get();
    }

    public Cartao ativarDesativarCartao(int id, AtivarDesativarCartaoDTO ativarDesativarCartaoDTO){
        Optional<Cartao> cartaoDB = cartaoRepository.findById(id);
        if (!cartaoDB.isPresent()) {
            throw new RuntimeException("Cartão não encontrado");
        }
        Cartao cartao = cartaoDB.get();

        //caso ativar
        if (ativarDesativarCartaoDTO.isAtivo()) {
            if (cartaoDB.get().isAtivo()) {
                throw new RuntimeException("Cartão já ativado");
            }
            cartao.setAtivo(true);
            return cartaoRepository.save(cartao);
        }

        //caso inativar
        if (!cartaoDB.get().isAtivo()) {
            throw new RuntimeException("Cartão já inativo");
        }
        cartao.setAtivo(false);
        return cartaoRepository.save(cartao);

    }

}
