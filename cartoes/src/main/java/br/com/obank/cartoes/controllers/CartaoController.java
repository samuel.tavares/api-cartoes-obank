package br.com.obank.cartoes.controllers;

import br.com.obank.cartoes.DTOs.AtivarDesativarCartaoDTO;
import br.com.obank.cartoes.models.Cartao;
import br.com.obank.cartoes.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cartao criarCartao(@RequestBody @Valid Cartao cartao){
        return cartaoService.criarCartao(cartao);
    }

    //caso necessario rever rota.
    /*@GetMapping("/{id}")
    public Cartao buscarCartaoPorId(@PathVariable(name = "id")int id){
        try {
            return cartaoService.buscarCartaoPorId(id);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }*/

    @GetMapping("/{numero}")
    public Cartao buscarCartaoPorNumero(@PathVariable(name = "numero")String numero){
        try {
            return cartaoService.buscarCartaoPorNumero(numero);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PatchMapping("/{id}")
    public Cartao ativarCartao(@RequestBody AtivarDesativarCartaoDTO ativarDesativarCartaoDTO, @PathVariable(name = "id")int id){
        try{
            return cartaoService.ativarDesativarCartao(id, ativarDesativarCartaoDTO);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
