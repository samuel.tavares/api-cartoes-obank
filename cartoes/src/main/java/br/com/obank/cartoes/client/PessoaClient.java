package br.com.obank.cartoes.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "PESSOA")
public interface PessoaClient {

    @GetMapping("/{id}")
    Pessoa buscarPorId(@PathVariable int id);
}
